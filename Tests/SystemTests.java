import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class SystemsTests {

    private CommandStorage commandStorage;
    private ErrorStorage errorStorage;
    private CreateValidator createValidator;
    private CreateProcessor createProcessor;
    private DepositProcessor depositProcessor;
    private WithdrawProcessor withdrawProcessor;

    @BeforeEach
    void setUp() {
        commandStorage = new CommandStorage();
        errorStorage = new ErrorStorage();
        createValidator = new CreateValidator(commandStorage);
        createProcessor = new CreateProcessor(commandStorage, errorStorage);
        depositProcessor = new DepositProcessor(commandStorage, errorStorage);
        withdrawProcessor = new WithdrawProcessor(commandStorage, errorStorage);
    }

    // CreateValidator Tests
    @Test
    void createValidator_validSavingsAccountCreation_shouldReturnTrue() {
        assertTrue(createValidator.validate("Create savings 12345678 1.0"));
    }

    @Test
    void createValidator_invalidAccountType_shouldReturnFalse() {
        assertFalse(createValidator.validate("Create gold 12345678 1.0"));
    }

    // CreateProcessor Tests
    @Test
    void createProcessor_validCreateCommand_shouldCreateAccount() {
        createProcessor.process("Create savings 12345678 1.0");
        assertNotNull(commandStorage.retrieveAccount("12345678"));
    }

    @Test
    void createProcessor_invalidCreateCommand_shouldLogError() {
        createProcessor.process("Create gold 12345678 1.0");
        assertFalse(errorStorage.getErrors().isEmpty());
    }

    // DepositProcessor Tests
    @Test
    void depositProcessor_validDepositCommand_shouldIncreaseBalance() {
        Savings account = new Savings("12345678", 0.5);
        commandStorage.storeAccount(account);
        depositProcessor.process("Deposit 12345678 500");
        assertEquals(500, account.getBalance());
    }

    @Test
    void depositProcessor_invalidAccountDepositCommand_shouldLogError() {
        depositProcessor.process("Deposit 87654321 500");
        assertFalse(errorStorage.getErrors().isEmpty());
    }

    // WithdrawProcessor Tests
    @Test
    void withdrawProcessor_validWithdrawCommand_shouldDecreaseBalance() {
        Savings account = new Savings("12345678", 0.5);
        account.deposit(1000);
        commandStorage.storeAccount(account);
        withdrawProcessor.process("Withdraw 12345678 500");
        assertEquals(500, account.getBalance());
    }

    @Test
    void withdrawProcessor_invalidAccountWithdrawCommand_shouldLogError() {
        withdrawProcessor.process("Withdraw 87654321 500");
        assertFalse(errorStorage.getErrors().isEmpty());
    }

    
}
