package main;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

class tests {
    List<String> input = new ArrayList<>();
	@Test
    void sample_make_sure_this_passes_unchanged_or_you_will_fail() {
        input.add("Create savings 12345678 0.6");
        input.add("Deposit 12345678 700");
        input.add("Deposit 12345678 5000");
        input.add("creAte cHecKing 98765432 0.01");
        input.add("Deposit 98765432 300");
        input.add("Transfer 98765432 12345678 300");
        input.add("Pass 1");
        input.add("Create cd 23456789 1.2 2000");
        List<String> actual = MasterControl.start(input);

        assertEquals(3, actual.size());
        assertEquals("Savings 12345678 1000.50 0.60", actual.get(0));
        assertEquals("Deposit 12345678 700", actual.get(1));
        assertEquals("Transfer 98765432 12345678 300", actual.get(2));
        assertEquals("Cd 23456789 2000.00 1.20", actual.get(3));
        assertEquals("Deposit 12345678 5000", actual.get(4));
    }
private Savings savings;
    private Checking checking;
    private CD cd;
    private CommandProcessor commandProcessor;
    private CommandStorage commandStorage;
    private ErrorStorage errorStorage;
    private DepositValidator depositValidator;
    private WithdrawValidator withdrawValidator;
    private Savings savingsAccount;
    private Checking checkingAccount;
    private MasterControl masterControl;
    private TransferValidator transferValidator;
    private TransferProcessor transferProcessor;
    private PassValidator passValidator;
    private PassProcessor passProcessor;

    @BeforeEach
    public void setUp() {
        commandStorage = new CommandStorage();
        errorStorage = new ErrorStorage();
        savingsAccount = new Savings("12345678", 0.5);
        checkingAccount = new Checking("87654321", 1.0);
        commandStorage.storeAccount(savingsAccount);
        commandStorage.storeAccount(checkingAccount);
        depositValidator = new DepositValidator(commandStorage);
        withdrawValidator = new WithdrawValidator(commandStorage);
        savings = new Savings("12345678", 0.5);
        checking = new Checking("87654321", 1.0);
        cd = new CD("23456789", 1.5, 1000);
        commandStorage = new CommandStorage();
        errorStorage = new ErrorStorage();
        commandProcessor = new CommandProcessor(commandStorage, errorStorage);
        masterControl = new MasterControl(commandProcessor, commandStorage, errorStorage);
        transferValidator = new TransferValidator(commandStorage);
        transferProcessor = new TransferProcessor(commandStorage, errorStorage);
        passValidator = new PassValidator();
        passProcessor = new PassProcessor(commandStorage, errorStorage);
    }

    // Savings Tests
    @Test
    void savings_depositWithinLimit_shouldIncreaseBalance() {
        savings.deposit(1000);
        assertEquals(1000, savings.getBalance());
    }

    @Test
    void savings_withdrawNotExceedingLimit_shouldDecreaseBalance() {
        savings.deposit(2000);
        savings.withdraw(1000);
        assertEquals(1000, savings.getBalance());
    }

    // Checking Tests
    @Test
    void checking_depositPositiveAmount_shouldIncreaseBalance() {
        checking.deposit(500);
        assertEquals(500, checking.getBalance());
    }

    @Test
    void checking_withdrawWithSufficientBalance_shouldDecreaseBalance() {
        checking.deposit(500);
        checking.withdraw(300);
        assertEquals(200, checking.getBalance());
    }

    // CD Tests
    @Test
    void cd_withdrawAfterMaturity_shouldAllowWithdrawal() {
        // Assume maturity is reached
        cd.withdraw(1000);
        assertEquals(0, cd.getBalance());
    }

    // CommandProcessor Tests
    @Test
    void commandProcessor_createSavingsAccount_shouldCreateAccount() {
        commandProcessor.process("create savings 12345678 0.5");
        assertNotNull(commandStorage.retrieveAccount("12345678"));
    }

    @Test
    void commandProcessor_depositCommand_shouldIncreaseAccountBalance() {
        commandProcessor.process("create checking 87654321 0.25");
        commandProcessor.process("deposit 87654321 500");
        BankAccount account = commandStorage.retrieveAccount("87654321");
        assertNotNull(account);
        assertEquals(500, account.getBalance());
    }
    // CommandStorage Tests
    @Test
    public void commandStorage_storeAndRetrieveAccount() {
        BankAccount retrievedSavings = commandStorage.retrieveAccount("12345678");
        assertNotNull(retrievedSavings);
        assertEquals(savingsAccount, retrievedSavings);

        BankAccount retrievedChecking = commandStorage.retrieveAccount("87654321");
        assertNotNull(retrievedChecking);
        assertEquals(checkingAccount, retrievedChecking);
    }

    // ErrorStorage Tests
    @Test
    public void errorStorage_logAndRetrieveErrors() {
        String errorMessage = "Test error message";
        errorStorage.logError(errorMessage);
        assertFalse(errorStorage.getErrors().isEmpty());
        assertTrue(errorStorage.getErrors().contains(errorMessage));
    }

    // DepositValidator Tests
    @Test
    public void depositValidator_validateValidDeposit() {
        assertTrue(depositValidator.validate("Deposit 12345678 100"));
    }

    @Test
    public void depositValidator_validateInvalidDeposit() {
        assertFalse(depositValidator.validate("Deposit 12345678 9999"));
    }

    // WithdrawValidator Tests
    @Test
    public void withdrawValidator_validateValidWithdraw() {
        savingsAccount.deposit(1000);
        assertTrue(withdrawValidator.validate("Withdraw 12345678 500"));
    }

    @Test
    public void withdrawValidator_validateInvalidWithdraw() {
        assertFalse(withdrawValidator.validate("Withdraw 12345678 9999"));
    }
    // MasterControl Tests
    @Test
    public void masterControl_executeCommands_shouldProcessCorrectly() {
        masterControl.addCommand("Create savings 12345678 0.5");
        masterControl.addCommand("Deposit 12345678 500");
        List<String> output = masterControl.start();
        assertEquals(1, output.size()); // Assuming only valid commands are added to output
        assertTrue(output.get(0).contains("Savings"));
    }

    // TransferValidator Tests
    @Test
    public void transferValidator_validateValidTransfer_shouldReturnTrue() {
        commandStorage.storeAccount(new Checking("87654321", 1.0));
        commandStorage.storeAccount(new Savings("12345678", 0.5));
        commandStorage.retrieveAccount("87654321").deposit(500);
        assertTrue(transferValidator.validate("Transfer 87654321 12345678 250"));
    }

    @Test
    public void transferValidator_validateInvalidTransfer_shouldReturnFalse() {
        assertFalse(transferValidator.validate("Transfer 12345678 87654321 1000000")); // Assuming invalid due to funds
    }

    // TransferProcessor Tests
    @Test
    public void transferProcessor_processValidTransfer_shouldTransferFunds() {
        commandStorage.storeAccount(new Checking("87654321", 1.0));
        commandStorage.storeAccount(new Savings("12345678", 0.5));
        commandStorage.retrieveAccount("87654321").deposit(500);
        transferProcessor.process("Transfer 87654321 12345678 250");
        assertEquals(250, commandStorage.retrieveAccount("12345678").getBalance());
    }

    // PassValidator Tests
    @Test
    public void passValidator_validateValidPass_shouldReturnTrue() {
        assertTrue(passValidator.validate("Pass 12"));
    }

    @Test
    public void passValidator_validateInvalidPass_shouldReturnFalse() {
        assertFalse(passValidator.validate("Pass 123")); // Assuming invalid due to out-of-range
    }

    // PassProcessor Tests
    @Test
    public void passProcessor_processPassTime_shouldCalculateInterest() {
        Savings testSavings = new Savings("12345678", 1.2);
        commandStorage.storeAccount(testSavings);
        testSavings.deposit(1000);
        passProcessor.process("Pass 12"); // Pass time for 12 months
        assertTrue(testSavings.getBalance() > 1000); // Balance should have increased due to interest
    }
}

