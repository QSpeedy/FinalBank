package main;
public interface BankAccount {
    String getId();
    void deposit(double amount);
    void withdraw(double amount);
    double getBalance();
    void calculateAPR();
    @Override
    String toString();
}
