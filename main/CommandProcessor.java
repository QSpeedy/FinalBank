package main;
import java.util.HashMap;
import java.util.Map;

public class CommandProcessor {
    private Map<String, Processor> processors;
    private ErrorStorage errorStorage;

    public CommandProcessor(CommandStorage commandStorage, ErrorStorage errorStorage) {
        this.errorStorage = errorStorage;
        this.processors = new HashMap<>();
        this.processors.put("create", new CreateProcessor(commandStorage, errorStorage));
        this.processors.put("deposit", new DepositProcessor(commandStorage, errorStorage));
        this.processors.put("withdraw", new WithdrawProcessor(commandStorage, errorStorage));
        this.processors.put("transfer", new TransferProcessor(commandStorage, errorStorage));
        this.processors.put("pass", new PassProcessor(commandStorage, errorStorage));
    }

    public void process(String command) {
        String[] parts = command.trim().split("\\s+");
        String commandType = parts[0].toLowerCase();
        Processor processor = processors.get(commandType);

        if (processor != null) {
            processor.process(command);
        } else {
            errorStorage.logError("Invalid command: " + command);
        }
    }
}
