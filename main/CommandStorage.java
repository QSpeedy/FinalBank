package main;
import java.util.HashMap;
import java.util.Map;
import java.util.Collection;

public class CommandStorage {
    private Map<String, BankAccount> accounts = new HashMap<>();

    public void storeAccount(BankAccount account) {
        accounts.put(account.getId(), account);
    }

    public BankAccount retrieveAccount(String accountId) {
        return accounts.get(accountId);
    }

    public Collection<BankAccount> getAllAccounts() {
        return accounts.values();
    }

    
}
