package main;

public class CreateProcessor implements Processor {
    private CommandStorage commandStorage;
    private ErrorStorage errorStorage;

    public CreateProcessor(CommandStorage commandStorage, ErrorStorage errorStorage) {
        this.commandStorage = commandStorage;
        this.errorStorage = errorStorage;
    }

    @Override
    public void process(String command) {
        String[] parts = command.split(" ");
        String accountType = parts[1];
        String id = parts[2];
        double apr = Double.parseDouble(parts[3].replace("%", ""));

        if (commandStorage.retrieveAccount(id) != null) {
            errorStorage.logError("Account with ID " + id + " already exists.");
            return;
        }

        BankAccount account;
        switch (accountType.toLowerCase()) {
            case "checking":
                account = new Checking(id, apr);
                break;
            case "savings":
                account = new Savings(id, apr);
                break;
            case "cd":
                double amount = Double.parseDouble(parts[4]);
                account = new CD(id, apr, amount);
                break;
            default:
                errorStorage.logError("Invalid account type: " + accountType);
                return;
        }
        commandStorage.storeAccount(account);
    }
}

