package main;
public class CreateValidator implements Validator {
    private CommandStorage commandStorage;

    public CreateValidator(CommandStorage commandStorage) {
        this.commandStorage = commandStorage;
    }

    @Override
    public boolean validate(String command) {
        String[] parts = command.split(" ");
        if (parts.length < 3 || parts.length > 4) return false;

        String accountType = parts[1].toLowerCase();
        if (!accountType.equals("checking") && !accountType.equals("savings") && !accountType.equals("cd")) {
            return false;
        }

        String id = parts[2];
        if (id.length() != 8 || commandStorage.retrieveAccount(id) != null) {
            return false;
        }

        try {
            double apr = Double.parseDouble(parts[3]);
            if (apr < 0 || apr > 10) return false;
        } catch (NumberFormatException e) {
            return false;
        }

        if (accountType.equals("cd")) {
            double amount = Double.parseDouble(parts[4]);
            if (amount < 1000 || amount > 10000) {
                return false;
            }
        }

        return true;
    }
}
