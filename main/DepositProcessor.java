package main;
public class DepositProcessor implements Processor {
    private CommandStorage commandStorage;
    private ErrorStorage errorStorage;

    public DepositProcessor(CommandStorage commandStorage, ErrorStorage errorStorage) {
        this.commandStorage = commandStorage;
        this.errorStorage = errorStorage;
    }

    @Override
    public void process(String command) {
        String[] parts = command.split(" ");
        String id = parts[1];
        double amount = Double.parseDouble(parts[2]);

        BankAccount account = commandStorage.retrieveAccount(id);
        if (account == null) {
            errorStorage.logError("Account with ID " + id + " not found.");
            return;
        }

        if (account instanceof CD) {
            errorStorage.logError("Cannot deposit into a CD account.");
            return;
        }

        account.deposit(amount);
    }
}
