package main;

import java.util.ArrayList;
import java.util.List;

public class ErrorStorage {
    private List<String> errors = new ArrayList<>();

    public void logError(String error) {
        errors.add(error);
    }

    public List<String> getErrors() {
        return new ArrayList<>(errors); 
    }
}
