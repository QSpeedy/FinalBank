package main;
import java.util.ArrayList;
import java.util.List;

public class MasterControl {
    private CommandProcessor commandProcessor;
    private CommandStorage commandStorage;
    private ErrorStorage errorStorage;
    private List<String> inputCommands;

    public MasterControl(CommandProcessor commandProcessor, CommandStorage commandStorage, ErrorStorage errorStorage) {
        this.commandProcessor = commandProcessor;
        this.commandStorage = commandStorage;
        this.errorStorage = errorStorage;
        this.inputCommands = new ArrayList<>();

       
        initializeCommands();
    }

    private void initializeCommands() {
        
        inputCommands.add("Create savings 12345678 0.6");
        inputCommands.add("Deposit 12345678 700");
        inputCommands.add("Deposit 12345678 5000");
        inputCommands.add("creAte cHecKing 98765432 0.01");
        inputCommands.add("Deposit 98765432 300");
        inputCommands.add("Transfer 98765432 12345678 300");
        inputCommands.add("Pass 1");
        inputCommands.add("Create cd 23456789 1.2 2000");
    }

    public List<String> start() {
        List<String> output = new ArrayList<>();

       
        for (String command : inputCommands) {
            commandProcessor.process(command);
        }

        
        for (BankAccount account : commandStorage.getAllAccounts()) {
            output.add(account.toString());
        }

        
        output.addAll(errorStorage.getErrors());

        return output;
    }

    
}
