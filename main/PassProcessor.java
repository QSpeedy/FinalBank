package main;
public class PassProcessor implements Processor {
    private CommandStorage commandStorage;
    private ErrorStorage errorStorage;

    public PassProcessor(CommandStorage commandStorage, ErrorStorage errorStorage) {
        this.commandStorage = commandStorage;
        this.errorStorage = errorStorage;
    }

    @Override
    public void process(String command) {
        String[] parts = command.split(" ");
        int months;
        try {
            months = Integer.parseInt(parts[1]);
            if (months < 1 || months > 60) {
                throw new IllegalArgumentException("Months must be between 1 and 60.");
            }
        } catch (NumberFormatException e) {
            errorStorage.logError("Invalid format for number of months: " + parts[1]);
            return;
        } catch (IllegalArgumentException e) {
            errorStorage.logError(e.getMessage());
            return;
        }

        for (BankAccount account : commandStorage.getAllAccounts()) {
            try {
                for (int i = 0; i < months; i++) {
                    account.calculateAPR();
                }
            } catch (Exception e) {
                errorStorage.logError("Error processing pass time for account " + account.getId() + ": " + e.getMessage());
            }
        }
    }
}


