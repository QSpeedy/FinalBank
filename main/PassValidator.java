package main;
public class PassValidator implements Validator {
    @Override
    public boolean validate(String command) {
        String[] parts = command.split(" ");
        if (parts.length != 2) return false;

        try {
            int months = Integer.parseInt(parts[1]);
            if (months < 1 || months > 60) return false;
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }
}

