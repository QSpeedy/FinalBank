package main;
public class Savings implements BankAccount {
    private String id;
    private double balance;
    private double apr; 
    private boolean hasWithdrawnThisMonth;

    public Savings(String id, double apr) {
        this.id = id;
        this.apr = apr;
        this.balance = 0.0;
        this.hasWithdrawnThisMonth = false;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void deposit(double amount) {
        if (amount <= 2500) {
            this.balance += amount;
        } else {
           
        }
    }

    @Override
    public void withdraw(double amount) {
        if (amount <= 1000 && !hasWithdrawnThisMonth) {
            this.balance = Math.max(0, this.balance - amount); 
            hasWithdrawnThisMonth = true;
        } else {
          
        }
    }

    @Override
    public double getBalance() {
        return this.balance;
    }

    @Override
    public void calculateAPR() {
        double monthlyInterestRate = (this.apr / 100) / 12;
        this.balance += this.balance * monthlyInterestRate;
    }

    public void resetWithdrawalFlag() {
        this.hasWithdrawnThisMonth = false;
    }

    @Override
    public String toString() {
        return String.format("Savings %s %.2f %.2f%%", getId(), getBalance(), apr);
    }
}

