package main;
import java.time.LocalDate;

public class CD implements BankAccount {
    private String id;
    private double balance;
    private double apr;
    private LocalDate creationDate;

    public CD(String id, double apr, double initialDeposit) {
        this.id = id;
        this.apr = apr;
        this.balance = initialDeposit;
        this.creationDate = LocalDate.now();
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void deposit(double amount) {
        
    }

    @Override
    public void withdraw(double amount) {
       
        if (LocalDate.now().isAfter(this.creationDate.plusMonths(12))) {
            if (amount == this.balance) {
                this.balance = 0;
            } else {
                
            }
        } else {
            
        }
    }

    @Override
    public double getBalance() {
        return this.balance;
    }

    @Override
    public void calculateAPR() {
        
        double quarterlyInterestRate = (this.apr / 100) / 4;
        this.balance += this.balance * quarterlyInterestRate;
    }

    @Override
    public String toString() {
        return String.format("CD %s %.2f %.2f%%", getId(), getBalance(), apr);
    }
}
