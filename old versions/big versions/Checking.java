package main;
public class Checking implements BankAccount {
    private String id;
    private double balance;
    private double apr; 

    public Checking(String id, double apr) {
        this.id = id;
        this.balance = 0; 
        this.apr = apr;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void deposit(double amount) {
       
        this.balance += amount;
    }

    @Override
    public void withdraw(double amount) {
       
        if (amount > this.balance) {
            this.balance = 0;
        } else {
            this.balance -= amount;
        }
    }

    @Override
    public double getBalance() {
        return this.balance;
    }

    @Override
    public void calculateAPR() {
        double monthlyInterestRate = (this.apr / 100) / 12;
        this.balance += this.balance * monthlyInterestRate;
    }

    @Override
    public String toString() {
        return String.format("Checking %s %.2f %.2f%%", getId(), getBalance(), apr);
    }
}
