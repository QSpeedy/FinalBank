package main;
public class DepositValidator implements Validator {
    private CommandStorage commandStorage;

    public DepositValidator(CommandStorage commandStorage) {
        this.commandStorage = commandStorage;
    }

    @Override
    public boolean validate(String command) {
        String[] parts = command.split(" ");
        if (parts.length != 3) return false;

        String id = parts[1];
        BankAccount account = commandStorage.retrieveAccount(id);
        if (account == null) return false;

        try {
            double amount = Double.parseDouble(parts[2]);
            if (account instanceof Savings && amount > 2500) return false;
            if (account instanceof Checking && amount > 1000) return false;
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }
}
