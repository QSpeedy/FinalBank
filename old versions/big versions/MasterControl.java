package main;

import java.util.ArrayList;
import java.util.List;

public class MasterControl {
    private CommandProcessor commandProcessor;
    private CommandStorage commandStorage;
    private ErrorStorage errorStorage;

    public MasterControl(CommandProcessor commandProcessor, CommandStorage commandStorage, ErrorStorage errorStorage) {
        this.commandProcessor = commandProcessor;
        this.commandStorage = commandStorage;
        this.errorStorage = errorStorage;
    }

    public List<String> start(List<String> inputCommands) {
        List<String> output = new ArrayList<>();

        for (String command : inputCommands) {
            commandProcessor.process(command);
        }

        
        for (BankAccount account : commandStorage.getAllAccounts()) {
            output.add(account.toString()); 
        }

        
        output.addAll(errorStorage.getErrors());

        return output;
    }
}
