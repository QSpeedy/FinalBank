package main;

public class TransferProcessor implements Processor {
    private CommandStorage commandStorage;
    private ErrorStorage errorStorage;

    public TransferProcessor(CommandStorage commandStorage, ErrorStorage errorStorage) {
        this.commandStorage = commandStorage;
        this.errorStorage = errorStorage;
    }

    @Override
    public void process(String command) {
        String[] parts = command.split(" ");
        String fromId = parts[1];
        String toId = parts[2];
        double amount = Double.parseDouble(parts[3]);

        BankAccount fromAccount = commandStorage.retrieveAccount(fromId);
        BankAccount toAccount = commandStorage.retrieveAccount(toId);

        if (fromAccount == null || toAccount == null) {
            errorStorage.logError("One or both accounts not found.");
            return;
        }

        fromAccount.withdraw(amount);
        toAccount.deposit(amount);
    }
}

