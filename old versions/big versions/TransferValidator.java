package main;
public class TransferValidator implements Validator {
    private CommandStorage commandStorage;

    public TransferValidator(CommandStorage commandStorage) {
        this.commandStorage = commandStorage;
    }

    @Override
    public boolean validate(String command) {
        String[] parts = command.split(" ");
        if (parts.length != 4) return false;

        String fromId = parts[1];
        String toId = parts[2];
        BankAccount fromAccount = commandStorage.retrieveAccount(fromId);
        BankAccount toAccount = commandStorage.retrieveAccount(toId);

        if (fromAccount == null || toAccount == null) return false;
        if (fromAccount instanceof CD || toAccount instanceof CD) return false;

        try {
            double amount = Double.parseDouble(parts[3]);
            if (amount > fromAccount.getBalance()) return false;
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }
}
