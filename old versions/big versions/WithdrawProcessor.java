package main;

public class WithdrawProcessor implements Processor {
    private CommandStorage commandStorage;
    private ErrorStorage errorStorage;

    public WithdrawProcessor(CommandStorage commandStorage, ErrorStorage errorStorage) {
        this.commandStorage = commandStorage;
        this.errorStorage = errorStorage;
    }

    @Override
    public void process(String command) {
        String[] parts = command.split(" ");
        String id = parts[1];
        double amount = Double.parseDouble(parts[2]);

        BankAccount account = commandStorage.retrieveAccount(id);
        if (account == null) {
            errorStorage.logError("Account with ID " + id + " not found.");
            return;
        }

        account.withdraw(amount);
    }
}

