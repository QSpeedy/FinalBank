package main;
public class WithdrawValidator implements Validator {
    private CommandStorage commandStorage;

    public WithdrawValidator(CommandStorage commandStorage) {
        this.commandStorage = commandStorage;
    }

    @Override
    public boolean validate(String command) {
        String[] parts = command.split(" ");
        if (parts.length != 3) return false;

        String id = parts[1];
        BankAccount account = commandStorage.retrieveAccount(id);
        if (account == null) return false;

        try {
            double amount = Double.parseDouble(parts[2]);
            if (account instanceof Savings && amount > 1000) return false;
            if (account instanceof Checking && amount > 400) return false;
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }
}
