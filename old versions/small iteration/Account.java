
import java.util.ArrayList;
import java.util.List;

public abstract class Account {
    protected String id;
    protected double balance;
    protected double apr;
    protected List<String> transactionHistory = new ArrayList<>();
    private static int creationCounter = 0;
    private final int creationOrder;
    protected boolean isActive;

    public Account(String id, double apr) {
        this.id = id;
        this.apr = apr;
        this.balance = 0;
        this.isActive = true;
        creationOrder = creationCounter++;
    }

    public abstract void deposit(double amount) throws TransactionException;
    public abstract void withdraw(double amount) throws TransactionException;
    public abstract void passTime(int months);

    public List<String> getTransactionHistory() {
        return new ArrayList<>(transactionHistory);
    }

    public int getCreationOrder() {
        return creationOrder;
    }

    public boolean isActive() {
        return isActive;
    }

    @Override
    public String toString() {
        return String.format("Account %s: Balance - %.2f, APR - %.2f", id, balance, apr);
    }
}
