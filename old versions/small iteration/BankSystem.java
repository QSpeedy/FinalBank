import java.util.*;

public class BankSystem {
    private Map<String, Account> accounts = new HashMap<>();
    private List<String> invalidCommands = new ArrayList<>();

    public List<String> start(List<String> commands) {
        for (String command : commands) {
            try {
                processCommand(command);
            } catch (InvalidCommandException | TransactionException e) {
                System.err.println("Error processing command: " + command + ". " + e.getMessage());
                invalidCommands.add(command);
            }
        }
        return generateOutput();
    }

    private void processCommand(String command) throws InvalidCommandException, TransactionException {
        String[] parts = command.toLowerCase().split("\\s+");
        String commandType = parts[0];

        switch (commandType) {
            case "create":
                createAccount(Arrays.copyOfRange(parts, 1, parts.length));
                break;
            case "deposit":
                deposit(Arrays.copyOfRange(parts, 1, parts.length));
                break;
            case "withdraw":
                withdraw(Arrays.copyOfRange(parts, 1, parts.length));
                break;
            case "transfer":
                transfer(Arrays.copyOfRange(parts, 1, parts.length));
                break;
            case "pass":
                passTime(Arrays.copyOfRange(parts, 1, parts.length));
                break;
            default:
                throw new InvalidCommandException("Invalid command: " + command);
        }
    }

    private void createAccount(String[] args) throws InvalidCommandException {
        if (args.length < 2 || args.length > 4) {
            throw new InvalidCommandException("Invalid number of arguments for create command");
        }

        String accountType = args[0];
        String id = args[1];
        double apr = Double.parseDouble(args[2]);

        if (accounts.containsKey(id)) {
            throw new InvalidCommandException("Account with ID already exists");
        }

        Account newAccount;
        switch (accountType) {
            case "savings":
                newAccount = new SavingsAccount(id, apr);
                break;
            case "checking":
                newAccount = new CheckingAccount(id, apr);
                break;
            case "cd":
                if (args.length != 4) {
                    throw new InvalidCommandException("Invalid number of arguments for CD account creation");
                }
                double initialAmount = Double.parseDouble(args[3]);
                newAccount = new CDAccount(id, apr, initialAmount);
                break;
            default:
                throw new InvalidCommandException("Invalid account type");
        }

        accounts.put(id, newAccount);
    }

    private void deposit(String[] args) throws InvalidCommandException, TransactionException {
        if (args.length != 3) {
            throw new InvalidCommandException("Invalid number of arguments for deposit command");
        }

        String id = args[1];
        double amount = Double.parseDouble(args[2]);

        Account account = accounts.get(id);
        if (account == null) {
            throw new InvalidCommandException("Account not found");
        }

        account.deposit(amount);
    }

    private void withdraw(String[] args) throws InvalidCommandException, TransactionException {
        if (args.length != 3) {
            throw new InvalidCommandException("Invalid number of arguments for withdraw command");
        }

        String id = args[1];
        double amount = Double.parseDouble(args[2]);

        Account account = accounts.get(id);
        if (account == null) {
            throw new InvalidCommandException("Account not found");
        }

        account.withdraw(amount);
    }

    private void transfer(String[] args) throws InvalidCommandException, TransactionException {
        if (args.length != 4) {
            throw new InvalidCommandException("Invalid number of arguments for transfer command");
        }

        String fromId = args[1];
        String toId = args[2];
        double amount = Double.parseDouble(args[3]);

        Account fromAccount = accounts.get(fromId);
        Account toAccount = accounts.get(toId);

        if (fromAccount == null || toAccount == null) {
            throw new InvalidCommandException("Account not found");
        }

        fromAccount.withdraw(amount);
        toAccount.deposit(amount);
    }

    private void passTime(String[] args) throws InvalidCommandException {
        if (args.length != 2) {
            throw new InvalidCommandException("Invalid number of arguments for pass time command");
        }

        int months = Integer.parseInt(args[1]);
        for (Account account : accounts.values()) {
            account.passTime(months);
        }
    }

    private List<String> generateOutput() {
        List<String> output = new ArrayList<>();
        for (String accountId : accounts.keySet()) {
            Account account = accounts.get(accountId);
            output.add(account.toString());
            output.addAll(account.getTransactionHistory());
        }
        output.addAll(invalidCommands);
        return output;
    }
}
