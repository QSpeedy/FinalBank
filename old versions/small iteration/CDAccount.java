import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class CDAccount extends Account {
    private LocalDate creationDate;
    private static final int CD_DURATION_MONTHS = 12;

    public CDAccount(String id, double apr, double initialAmount) {
        super(id, apr);
        this.balance = initialAmount;
        this.creationDate = LocalDate.now();
    }

    @Override
    public void deposit(double amount) throws TransactionException {
        throw new TransactionException("Deposits are not allowed in CD Accounts.");
    }

    @Override
    public void withdraw(double amount) throws TransactionException {
        if (ChronoUnit.MONTHS.between(creationDate, LocalDate.now()) >= CD_DURATION_MONTHS) {
            balance = 0;
            isActive = false;
            transactionHistory.add("Withdrawal: " + amount);
        } else {
            throw new TransactionException("Withdrawal from CD Account is not allowed yet.");
        }
    }

    @Override
    public void passTime(int months) {
        for (int i = 0; i < months; i++) {
            for (int j = 0; j < 4; j++) { 
                if (isActive) {
                    balance += balance * (apr / 100) / 48; 
                }
            }
        }
    }
}
