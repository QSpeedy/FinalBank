public class CheckingAccount extends Account {
    private static final double MAX_DEPOSIT = 1000;
    private static final double MAX_WITHDRAWAL = 400;

    public CheckingAccount(String id, double apr) {
        super(id, apr);
    }

    @Override
    public void deposit(double amount) throws TransactionException {
        if (amount <= MAX_DEPOSIT) {
            balance += amount;
            transactionHistory.add("Deposit: " + amount);
        } else {
            throw new TransactionException("Deposit amount exceeds the maximum limit for Checking Account.");
        }
    }

    @Override
    public void withdraw(double amount) throws TransactionException {
        if (amount <= MAX_WITHDRAWAL) {
            balance -= Math.min(amount, balance); 
            transactionHistory.add("Withdrawal: " + amount);
        } else {
            throw new TransactionException("Withdrawal request exceeds limits for Checking Account.");
        }
    }

    @Override
    public void passTime(int months) {
        for (int i = 0; i < months; i++) {
            if (balance > 0) {
                balance += balance * (apr / 100) / 12;
            }
        }
    }
}
