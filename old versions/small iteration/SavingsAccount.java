public class SavingsAccount extends Account {
    private static final double MAX_DEPOSIT = 2500;
    private static final double MAX_WITHDRAWAL = 1000;
    private int withdrawalCount = 0;

    public SavingsAccount(String id, double apr) {
        super(id, apr);
    }

    @Override
    public void deposit(double amount) throws TransactionException {
        if (amount <= MAX_DEPOSIT) {
            balance += amount;
            transactionHistory.add("Deposit: " + amount);
        } else {
            throw new TransactionException("Deposit amount exceeds the maximum limit for Savings Account.");
        }
    }

    @Override
    public void withdraw(double amount) throws TransactionException {
        if (withdrawalCount < 1 && amount <= MAX_WITHDRAWAL) {
            balance -= Math.min(amount, balance); 
            transactionHistory.add("Withdrawal: " + amount);
            withdrawalCount++;
        } else {
            throw new TransactionException("Withdrawal request exceeds limits for Savings Account.");
        }
    }

    @Override
    public void passTime(int months) {
        for (int i = 0; i < months; i++) {
            if (balance > 0) {
                balance += balance * (apr / 100) / 12;
            }
            withdrawalCount = 0;
        }
    }
}
